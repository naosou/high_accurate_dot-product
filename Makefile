#SYSTEM = INTEL
SYSTEM = GNU
#SYSTEM=INTELdebug
#SYSTEM=GNUdebug

#intel
ifeq ($(SYSTEM),INTEL)
F90=mpiifort
OPTFLAGS = -O3 -xHost -align array64byte
LDFLAGS = -qopenmp -mkl -ipo
FFLAGS = $(OPTFLAGS) $(LDFLAGS) -stand f03 -fpp
endif

#intel
ifeq ($(SYSTEM),INTELdebug)
F90=mpiifort
DFLAGS = -check all -g -traceback
LDFLAGS = -qopenmp -mkl
FFLAGS = $(DFLAGS) $(LDFLAGS) -stand f03
endif

#GNU
ifeq ($(SYSTEM),GNU)
F90=mpif90
OPTFLAGS= -O3
LDFLAGS= -fopenmp
FFLAGS = $(OPTFLAGS) $(LDFLAGS) -ffree-line-length-none -std=f2003 
endif

#GNU_debug
ifeq ($(SYSTEM),GNUdebug)
F90=mpif90
DFLAGS= -fbounds-check -Wuninitialized -ffpe-trap=invalid,zero,overflow -fbacktrace -g
LDFLAGS= -fopenmp
FFLAGS = $(DFLAGS) $(LDFLAGS) -ffree-line-length-none -std=f2003 
endif

TARGET = test_valid_comp
OBJECTS = valid_comp.o main.o

.SUFFIXES :
.SUFFIXES :.f90 .o

${TARGET} : ${OBJECTS}
	${F90} ${LDFLAGS} -o $@ ${OBJECTS}

.f90.o:
	${F90} ${FFLAGS} -c $<

clean : 
	rm -f ${OBJECTS}

