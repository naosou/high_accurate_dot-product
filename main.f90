program main
  use mod_valid_dotp
  implicit none
  integer i, N, K
  real(8) normal1, normal2, validated, tresult
  real(8) :: factor1 = 1.0d-100, factor2 = 1.2d0, max = 1.0d10, min = 1.0d-10
  real(8), allocatable :: array1(:), array2(:)
  character(10) :: line

  interface
     subroutine FY_shuffle(array1)
       implicit none
       real(8), intent(inout) :: array1(:)
     end subroutine FY_shuffle
  end interface
  
  call get_command_argument(1, line)
  read(line, *) N

  call get_command_argument(2, line)
  read(line, *) K

  allocate(array1(N), array2(N))

  ! do i = 1, n
  !    array1(i) = factor1 * (factor2 ** (i-1))
  ! enddo

  ! call FY_shuffle(array1)
  ! write(*, *) 'Finish creating vector'

  call  random_number(array1)
  array1(:) = (array1(:) * (max/min) + 1.0d0 ) * min
  call  random_number(array2)
  array2(:) = (array2(:) * (max/min) + 1.0d0 ) * min
  
  normal1 = dot_product(array1, array2)
  write(*, *) 'Finish normal dot-product'
  normal2 = validate_dotp(array1, array2, 1)
  write(*, *) 'Finish validated dot-product'
  ! tresult = (factor1 ** 2) * (factor2 ** (2*n) - 1) / (factor2 ** 2 - 1)
  validated = validate_dotp(array1, array2, K)
  
  ! write(*, '(3(a, e20.14))') 'Check result, normal = ', normal, ', Validated = ', validated, ', True = ', tresult
  write(*, '(3(a, e20.14))') 'Check result, normal = ', normal1, ', validated_debug = ', normal2, ', validated = ', validated

end program main

subroutine FY_shuffle(array)
  implicit none
  real(8), intent(inout) :: array(:)
  integer i, j, k, swap, N
  real(8) temp
  real(8), allocatable :: rands(:)

  N = ubound(array, 1)
  allocate(rands(N))
  call random_number(rands)
    
  do i = 1, N-1
     swap = rands(i)*(N-1) + 1
     ! write(*, *) 'Check swap', swap, rands(i)
     temp = array(swap)
     array(swap:N-1) = array(swap+1:N)
     array(N) = temp
  enddo

end subroutine FY_shuffle
