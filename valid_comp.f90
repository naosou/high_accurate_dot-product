module for_validation
  use, intrinsic :: ieee_arithmetic
  implicit none

!      call ieee_set_rounding_mode(ieee_nearest)   
!      call ieee_set_rounding_mode(ieee_up)
!      call ieee_set_rounding_mode(ieee_down)

  real(8) :: factor = 2 ** (52 / 2) + 1
  
contains

  function two_sum(a, b) result(xy)
    implicit none
    real(8), intent(in) :: a, b
    real(8) :: xy(2)
    real(8) c

    call ieee_set_rounding_mode(ieee_nearest)

    xy(1) = a + b
    c = xy(1) - a
    xy(2) = (a - (xy(1) - c)) + (b - c)
    
  end function two_sum

  function split(a) result(hl)
    implicit none
    real(8), intent(in) :: a
    real(8) :: hl(2)
    real(8) c

    call ieee_set_rounding_mode(ieee_nearest)

    c = factor * a
    hl(1) = c - (c - a)
    hl(2) = a - hl(1)
    
  end function split

  function two_product(a, b) result(xy)
    implicit none
    real(8), intent(in) :: a, b
    real(8) :: xy(2)
    real(8) :: ahl(2), bhl(2)

    call ieee_set_rounding_mode(ieee_nearest)

    xy(1) = a * b
    ahl = split(a)
    bhl = split(b)
    xy(2) = ahl(2) * bhl(2) - (((xy(1) - ahl(1) * bhl(1)) - ahl(2) * bhl(1)) &
                                                                - ahl(1) * bhl(2))
    
  end function two_product

  function vec_sum(p, n) result(pd)
    implicit none
    integer, intent(in) :: n
    real(8), intent(in) :: p(:)
    real(8) :: pd(n)
    integer i
    real(8), allocatable :: piq(:, :)
    
    allocate(piq(2, n))

    piq(1, 1) = p(1)
    do i = 2, n
       piq(:, i) = two_sum(piq(1, i-1), p(i))
    enddo

    pd(1:n-1) = piq(2, 2:n)
    pd(n) = piq(1, n)
    
    deallocate(piq)

  end function vec_sum

  function sum_k(p, k) result(sk)
    implicit none
    integer, intent(in) :: k
    real(8), intent(in) :: p(:)
    real(8) :: sk, tn
    integer i, n
    real(8), allocatable :: t(:)

    n = ubound(p, 1)
    allocate(t(n))

    t(:) = p(:)
    do i = 1, k-1
       t = vec_sum(t, n)
    enddo

    call ieee_set_rounding_mode(ieee_nearest)

    sk = t(n)
    do i = 1, n-1
       sk = sk + t(i)
    enddo
    
  end function sum_k
  
end module for_validation

module mod_valid_dotp

contains
  
  function validate_dotp(x, y, K) result(dotp)
    use for_validation
    implicit none
    integer, intent(in) :: K
    real(8), intent(in) :: x(:), y(:)
    real(8) :: dotp
    real(8) :: hr(2), pr(2)
    integer i, n
    real(8), allocatable :: r(:)

    n = ubound(x, 1)
    if(k > n) then
       write(*, *) 'K must be smalelr than the DoF of the vector'
       stop
    endif
    allocate(r(2*n))

    pr = two_product(x(1), y(1))
    r(1) = pr(2)
    
    do i = 2, n
       hr = two_product(x(i), y(i))
       r(i) = hr(2)
       pr = two_sum(pr(1), hr(1))
       r(n+i-1) = pr(2)
    enddo
    
    r(2*n) = pr(1)
    
    dotp = sum_k(r, k-1)
    
  end function validate_dotp

end module mod_valid_dotp
